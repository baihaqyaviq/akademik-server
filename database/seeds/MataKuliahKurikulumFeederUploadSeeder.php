<?php

use FE_UNSIQ\Eloquent\MataKuliahKurikulum;
use FE_UNSIQ\Services\Feeder\WSClient;
use Illuminate\Database\Seeder;

class MataKuliahKurikulumFeederUploadSeeder extends Seeder
{

    /**
     * @var WSClient
     */
    private $WSClient;

    public function __construct(WSClient $WSClient)
    {
        $this->WSClient = $WSClient;
    }

    public function run()
    {
        $mk_kurikulum_data = MataKuliahKurikulum::all()->toArray();
        $data = [];

        foreach ($mk_kurikulum_data as $mk_kurikulum) {
            unset($mk_kurikulum['id_mk_kurikulum']);
            unset($mk_kurikulum['last_update']);
            unset($mk_kurikulum['soft_delete']);
            unset($mk_kurikulum['last_sync']);
            unset($mk_kurikulum['id_updater']);
            $data[] = $mk_kurikulum;
        }

        dd($this->WSClient->insertRecordSet('mata_kuliah_kurikulum', $data));
    }
}