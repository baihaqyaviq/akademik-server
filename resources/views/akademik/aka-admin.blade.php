<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Akademik</title>

    <!-- Webix Library -->
    <script type="text/javascript" src="assets/js/webix.js?1468503889704"></script>
    <link rel="stylesheet" href="assets/css/material.css?1468503889704">
    <link rel="stylesheet" href="assets/css/app.css?1468503889704">
    <style type="text/css">
        .webixapp {
            transition: opacity 500ms;
            opacity: 1;
            background: #34495e !important;
        }
    </style>

    <script type="text/javascript" src="assets/js/js.cookie.js?1468503889704"></script>
    <script type="text/javascript" src="assets/js/jsrsasign-latest-all-min.js?1468503889704"></script>

    <!-- The app's logic -->
    <script type="text/javascript" src="admin/app.js?1468503889704"></script>
    <script type="text/javascript">
        webix.production = true; require.config({
            urlArgs: "bust=" + (new Date()).getTime(),
            /*paths: {
             "jspdf": "jspdf.min",
             "autotable": "jspdf.plugin.autotable"
             },
             shim: {
             "jspdf": {
             exports: "jsPDF"
             }
             }*/
        });
    </script>

</head>
<body></body>
</html>
