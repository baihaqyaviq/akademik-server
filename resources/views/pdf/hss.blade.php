<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c3 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 64.1pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c22 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 26.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c37 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 26.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c9 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 26.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c36 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c28 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 70pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c11 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 256.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c39 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c26 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c45 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c4 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c21 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 256.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c48 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c10 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c43 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c41 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 420.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c42 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 26.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c18 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 256.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c30 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 256.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c35 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c2 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c13 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 128.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c15 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 99.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c44 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c40 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c24 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c8 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c31 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Arial";
            font-style: normal
        }

        .c1 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c33 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Arial";
            font-style: normal
        }

        .c20 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Arial";
            font-style: normal
        }

        .c5 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center;
            height: 11pt
        }

        .c6 {
            margin-left: 288pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c27 {
            margin-left: auto;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c7 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c23 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c0 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c17 {
            font-size: 12pt;
            font-weight: 700;
            text-decoration: underline
        }

        .c32 {
            background-color: #ffffff;
            max-width: 538.6pt;
            padding: 28.3pt 28.3pt 28.3pt 28.3pt
        }

        .c25 {
            line-height: 1.0;
            text-align: center
        }

        .c34 {
            line-height: 1.0;
            text-align: left
        }

        .c12 {
            orphans: 2;
            widows: 2
        }

        .c19 {
            height: 20pt
        }

        .c29 {
            font-size: 10pt
        }

        .c46 {
            font-weight: 700
        }

        .c38 {
            line-height: 1.0
        }

        .c14 {
            height: 0pt
        }

        .c16 {
            height: 11pt
        }

        .c47 {
            height: 21pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c32">
<table class="c23">
    <tbody>
    <tr class="c47">
        <td class="c15" colspan="1" rowspan="1">
            <p class="c7"><span
                        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 97.00px; height: 92.33px;"><img
                            alt="LogoKRS.png" src="{{ URL::asset('assets/imgs/LogoKRS.png') }}"
                            style="width: 97.00px; height: 92.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                            title=""></span></p>
        </td>
        <td class="c41" colspan="1" rowspan="1">
            <p class="c0 c12"><span class="c33">PROGRAM PASCA SARJANA</span></p>
            <p class="c0 c12"><span class="c20">MAGISTER PENDIDIKAN ISLAM (M.Pd.I)</span></p>
            <p class="c0 c12"><span class="c20">MAGISTER STUDI ISLAM (M.S.I)</span></p>
            <p class="c0 c12"><span class="c31">UNIVERSITAS SAINS AL-QUR’AN</span></p>
            <p class="c0 c12"><span class="c24">JAWA TENGAH DI WONOSOBO</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c12 c38 c16"><span></span></p>
<p class="c25 c12"><span class="c17">HASIL STUDI SEMESTER {{ $semester_tempuh }}</span></p>
<p class="c34 c12 c16"><span class="c17"></span></p>
<table class="c27">
    <tbody>
    <tr class="c14">
        <td class="c43" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Nama Mahasiswa</span></p>
        </td>
        <td class="c36" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: {{ $mhs->nm_pd }}</span></p>
        </td>
        <td class="c28" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Semester Mhs</span></p>
        </td>
        <td class="c35" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: {{ $mhs->smt_mhs }}</span></p>
        </td>
    </tr>
    <tr class="c14">
        <td class="c45" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Nomor Induk Mahasiswa</span></p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: {{ $mhs->nipd }}</span></p>
        </td>
        <td class="c48" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Tahun Masuk</span></p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: {{ $mhs->mulai_smt }}</span></p>
        </td>
    </tr>
    <tr class="c14">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Program Studi</span></p>
        </td>
        <td class="c44" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: {{ $mhs->nm_lemb }}</span></p>
        </td>
        <td class="c39" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">Jenjang</span></p>
        </td>
        <td class="c2" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">: S2 (Magister)</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c12 c16 c34"><span class="c17"></span></p>
<table class="c27">
    <tbody>
    <tr class="c14">
        <td class="c9" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">NO</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">MATA KULIAH</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">SKS</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">HURUF</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">ANGKA</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">MUTU</span></p>
        </td>
    </tr>

    @foreach( $nilai as $i => $nilai_mahasiswa)
        {{--*/ $nilai_mutu = $nilai_mahasiswa->nilai_indeks * $nilai_mahasiswa->sks_mk /*--}}

        <tr class="c14">
            <td class="c9" colspan="1" rowspan="1">
                <p class="c7"><span class="c1">{{ $i + 1 }}</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c0"><span class="c1">{{ $nilai_mahasiswa->nm_mk }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c7"><span class="c1">{{ $nilai_mahasiswa->sks_mk }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c7"><span class="c1">{{ $nilai_mahasiswa->nilai_huruf }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c7"><span class="c1">{{ $nilai_mahasiswa->nilai_indeks }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c7"><span class="c1">{{ $nilai_mutu }}</span></p>
            </td>
        </tr>
    @endforeach

    <tr class="c14">
        <td class="c9" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">JUMLAH</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c1">{{ $ips->total_sks }}</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">&nbsp;</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c0"><span class="c1">&nbsp;</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c1">{{ $ips->total_nm }}</span></p>
        </td>
    </tr>
    <tr class="c19">
        <td class="c22" colspan="1" rowspan="1">
            <p class="c0 c16"><span class="c1"></span></p>
        </td>
        <td class="c21" colspan="1" rowspan="1">
            <p class="c5"><span class="c8"></span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c1">INDEKS PRESTASI (IP)</span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c8">{{ $ips->number }}</span></p>
        </td>
    </tr>
    <tr class="c19">
        <td class="c37" colspan="1" rowspan="1">
            <p class="c0 c16"><span class="c1"></span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c5"><span class="c8"></span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c1">IP KUMULATIF (IPK)</span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c8">{{ $ipk->number }}</span></p>
        </td>
    </tr>
    <tr class="c19">
        <td class="c42" colspan="1" rowspan="1">
            <p class="c0 c16"><span class="c1"></span></p>
        </td>
        <td class="c30" colspan="1" rowspan="1">
            <p class="c5"><span class="c8"></span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c1">PREDIKAT</span></p>
        </td>
        <td class="c13" colspan="2" rowspan="1">
            <p class="c7"><span class="c8"> {{ $ipk->predicate }} </span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c12 c16 c25"><span class="c17"></span></p>
<p class="c25 c12 c16"><span class="c17"></span></p>
<p class="c6"><span class="c29">Wonosobo, @showToday </span></p>
<p class="c6"><span class="c29">An. Direktur</span></p>
<p class="c6"><span class="c29">Ketua Program Studi</span></p>
<p class="c6 c16"><span class="c29"></span></p>
<p class="c6 c16"><span class="c29"></span></p>
<p class="c6 c16"><span class="c29"></span></p>
<p class="c6"><span class="c29 c46">{{ $mhs->kaprodi }}</span></p>
</body>

</html>