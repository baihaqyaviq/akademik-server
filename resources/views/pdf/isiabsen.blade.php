<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c25 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 375pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c15 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 100.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c14 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 640.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c23 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 377.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c1 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 45.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c17 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 206.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c13 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 375pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c16 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 100.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c5 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 377.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c20 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 75pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c21 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 96.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c24 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 96.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c7 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 27.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c2 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c3 {
            color: #000000;
            text-decoration: none;
            vertical-align: baseline;
            font-family: "Arial";
            font-style: normal
        }

        .c4 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c44 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c18 {
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c11 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c9 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: left
        }

        .c19 {
            margin-left: auto;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c12 {
            background-color: #ffffff;
            max-width: 951.3pt;
            padding: 28.3pt 28.3pt 28.3pt 28.3pt
        }

        .c22 {
            line-height: 1.0;
            orphans: 2;
            widows: 2
        }

        .c10 {
            font-size: 12pt;
            font-weight: 700;
            text-decoration: underline
        }

        .c0 {
            font-size: 10pt;
            font-weight: 700
        }

        .c88 {
            height: 11pt
        }

        .c6 {
            height: 0pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c12">

<p class="c18"><span class="c0">DAFTAR HADIR KULIAH PROGRAM PASCA SARJANA</span></p>
<p class="c18"><span class="c0">UNIVERSITAS SAINS AL-QUR'AN (UNSIQ) JAWA TENGAH DI WONOSOBO</span></p>
<p class="c18"><span class="c0">TAHUN AKADEMIK 2016/2017</span></p>
<p class="c88 c22"><span class="c10"></span></p>
<table class="c19">
    <tbody>
    <tr class="c6">
        <td class="c21" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">Dosen</span></p>
        </td>
        <td class="c23" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">:
                    @if(! is_null($dosen))
                        {{$dosen->nm_ptk}}
                    @endif
                        </span></p>
        </td>
        <td class="c15" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">Program Studi</span></p>
        </td>
        <td class="c13" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">: {{ $kelas->nm_lemb }}</span></p>
        </td>
    </tr>
    <tr class="c6">
        <td class="c24" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">Mata Kuliah</span></p>
        </td>
        <td class="c5" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">: {{ $kelas->nm_mk }} ({{ $kelas->sks_mk }} SKS)</span></p>
        </td>
        <td class="c16" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">Semester</span></p>
        </td>
        <td class="c25" colspan="1" rowspan="1">
            <p class="c11"><span class="c2">: 2</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c18 c88"><span class="c0"></span></p>
<table class="c19">
    <tbody>
    <tr class="c6">
        <td class="c7" colspan="1" rowspan="2">
            <p class="c4"><span class="c3 c0">NO</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="2">
            <p class="c4"><span class="c3 c0">NAMA MAHASISWA</span></p>
        </td>
        <td class="c20" colspan="1" rowspan="2">
            <p class="c4"><span class="c3 c0">NIM</span></p>
        </td>
        <td class="c14" colspan="14" rowspan="1">
            <p class="c4"><span class="c3 c0">PERTEMUAN</span></p>
        </td>
    </tr>
    <tr class="c6">
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">1</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">2</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">3</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">4</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">5</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">6</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">7</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">8</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">9</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">10</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">11</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">12</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">13</span></p>
        </td>
        <td class="c1" colspan="1" rowspan="1">
            <p class="c4"><span class="c3 c0">14</span></p>
        </td>
    </tr>
    @foreach($krs as $i => $mahasiswa)
        <tr class="c6">
            <td class="c7" colspan="1" rowspan="1">
                <p class="c4 c8"><span class="c3 c0">{{ $i + 1 }}</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c44 c8"><span class="c3 c0">{{ $mahasiswa->nm_pd }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c4 c8"><span class="c3 c0">{{ $mahasiswa->nipd }}</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c0 c3">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
            <td class="c1" colspan="1" rowspan="1">
                <p class="c4"><span class="c3 c0">&nbsp;</span></p>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>

</html>