<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad;

use FE_UNSIQ\Services\Webix\DynamicLoad\Contracts\DynamicLoadInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class DynamicLoader
 * The default dynamic loader
 *
 * @package FE_UNSIQ\Services\Webix\DynamicLoad
 */
class DynamicLoader implements DynamicLoadInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Builder
     */
    protected $queryBuilder;

    protected $totalCount;

    protected $defaultLimit = 25;

    /**
     * DynamicLoader constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Builder $queryBuilder
     */
    public function setQueryBuilder(Builder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return array
     */
    public function generateQueryBuilder()
    {
        return $this->queryBuilder->take($this->defaultLimit);
    }

}