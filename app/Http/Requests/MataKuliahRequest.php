<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class MataKuliahRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_sms' => 'required',
            'id_jenj_didik' => 'required',
            'kode_mk' => 'required',
            'nm_mk' => 'required',
            'jns_mk' => 'required',
            'kel_mk' => 'required',
            'sks_mk' => 'required|numeric',
            'tgl_mulai_efektif' => 'required',
            'tgl_akhir_efektif' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Matakuliah Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
