<?php

namespace FE_UNSIQ\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class Administrator extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! $token = $this->auth->setRequest($request)->getToken()) 
            return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
        } catch (JWTException $e) {
            return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }

        if (! $user) {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }

        if ( ! $user->hasRole('Administrator')) 
            return response()->json([
                'message' => 'This endpoint can only be accessed by Administrator',
                'status_code' => '403' 
            ], 403);

        return $next($request);
    }
}
