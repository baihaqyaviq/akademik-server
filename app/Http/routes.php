<?php

$api = app(Dingo\Api\Routing\Router::class);


Route::get('/', function () use ($api) {
    return view('akademik/mhs-akademik');
}, 200);

Route::get('/aka-admin', function () use ($api) {

    try {
        dd(\Tymon\JWTAuth\Facades\JWTAuth::parseToken()->authenticate());
    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        return view('akademik/aka-admin');
    }
}, 200);

$api->version('1.1.0', function () use ($api) {

    /*
    |--------------------------------------------------------------------------
    | Landing Page Endpoints
    |--------------------------------------------------------------------------
    |
     */
    # Guest routes...
    $api->group(['namespace' => '\FE_UNSIQ\Http\Controllers'], function () use ($api) {

        /*
        |--------------------------------------------------------------------------
        | Auth Endpoints
        |--------------------------------------------------------------------------
        |
         */

        $api->post('auth/generate-token', 'Auth\JwtAuthController@generateToken');

    });

    # Authemticated user routes...
    $api->group(['middleware' => 'api.auth', 'namespace' => '\FE_UNSIQ\Http\Controllers'], function () use ($api) {

        /*
        |--------------------------------------------------------------------------
        | Users Endpoints
        |--------------------------------------------------------------------------
        | - List (DataTable) : GET -> /users
        | - Show : GET -> /users/{id_user}
        |
         */

        $api->get('users', 'User\UsersController@index');
        $api->get('users/{user_d}', 'User\UsersController@show');

        /*
        |--------------------------------------------------------------------------
        | Mahasiswa Endpoints
        |--------------------------------------------------------------------------
        | - List : GET -> /mahasiswa
        | - Store : POST -> /mahasiswa
        | - Show : GET -> /mahasiswa/{mahasiswa_id}
        | - Update : PUT -> /mahasiswa/{mahasiswa_id}
        | - Krs Diambil [List] : GET -> /mahasiswa/{id_mahasiswa}/krs
        | - Krs Diambil [Store] : POST -> /mahasiswa/{id_mahasiswa}/krs
        | - Krs Diambil [Show] : GET -> /mahasiswa/{id_mahasiswa}/krs/{krs_id}
        | - Krs Diambil [Destroy] : DELETE -> /mahasiswa/{id_mahasiswa}/krs/{krs_id}
        | - Krs Ditawarkan [List] : GET -> /mahasiswa/{id_mahasiswa}/krs-ditawarkan
        | - Krs Her [List] : GET -> /mahasiswa/{id_mahasiswa}/krs-her
        | - Nilai [List] (DataTable) : GET -> /mahasiswa/{id_mahasiswa}/nilai
        | - Riwayat Pendidikan [List] : GET -> /mahasiswa/{id_mahasiswa}/riwayat-pendidikan
        | - Kuliah Mahasiswa [List] : GET -> /mahasiswa/{id_mahasiswa}/kuliah-mahasiswa
        | - Status Registrasi [List] : GET -> /mahasiswa/{id_mahasiswa}/status-registrasi
        |
         */

        $api->get('mahasiswa', 'Mahasiswa\MahasiswaController@mahasiswaIndex');
        $api->get('mahasiswa/grafik', 'Mahasiswa\MahasiswaController@mahasiswaGrafik');
        $api->post('mahasiswa', 'Mahasiswa\MahasiswaController@mahasiswaStore')->middleware('admin');
        $api->get('mahasiswa/{mahasiswa_id}', 'Mahasiswa\MahasiswaController@mahasiswaShow');
        $api->put('mahasiswa/{mahasiswa_id}', 'Mahasiswa\MahasiswaController@mahasiswaUpdate')->middleware('admin');
        $api->get('mahasiswa/{mahasiswa_id}/krs', 'Mahasiswa\KrsController@krsIndex');

        $api->get('mahasiswa/{mahasiswa_id}/krs-cetak', 'Mahasiswa\KrsController@krsCetak');
        $api->get('mahasiswa/{mahasiswa_id}/{smt_mahasiswa}/hss-cetak', 'Mahasiswa\NilaiController@hssCetak');
        $api->get('mahasiswa/{mahasiswa_id}/transkrip-cetak', 'Mahasiswa\NilaiController@transkripCetak');

        $api->post('mahasiswa/{mahasiswa_id}/krs', 'Mahasiswa\KrsController@krsStore');
        $api->get('mahasiswa/{mahasiswa_id}/krs/{krs_id}', 'Mahasiswa\KrsController@krsShow');
        $api->delete('mahasiswa/{mahasiswa_id}/krs/{krs_id}', 'Mahasiswa\KrsController@krsDestroy');
        $api->get('mahasiswa/{mahasiswa_id}/krs-ditawarkan', 'Mahasiswa\KrsController@krsDitawarkanIndex');
        $api->get('mahasiswa/{mahasiswa_id}/krs-diambil', 'Mahasiswa\KrsController@getKrsDiambil');
        $api->get('mahasiswa/{mahasiswa_id}/krs-her', 'Mahasiswa\KrsController@krsHerIndex');
        $api->get('mahasiswa/{mahasiswa_id}/nilai', 'Mahasiswa\NilaiController@nilaiIndex');
//        $api->post('mahasiswa/{mahasiswa_id}/nilai', 'Mahasiswa\NilaiController@nilaiStore');
        $api->get('mahasiswa/{mahasiswa_id}/nilai/{nilai_id}', 'Mahasiswa\NilaiController@nilaiShow');
        $api->put('mahasiswa/{mahasiswa_id}/nilai/{nilai_id}', 'Mahasiswa\NilaiController@nilaiUpdate')->middleware('admin');
        $api->delete('mahasiswa/{mahasiswa_id}/nilai/{nilai_id}', 'Mahasiswa\NilaiController@nilaiDestroy')->middleware('admin');
        $api->get('mahasiswa/{mahasiswa_id}/kuliah-mahasiswa', 'Mahasiswa\KuliahMahasiswaController@kuliahMahasiswaIndex');
        $api->get('mahasiswa/{mahasiswa_id}/riwayat-pendidikan', 'Mahasiswa\RiwayatPendidikanController@riwayatPendidikanIndex');
        $api->get('mahasiswa/{mahasiswa_id}/status-registrasi', 'Mahasiswa\StatusRegistrasiController@statusRegistrasiIndex');
        $api->get('mahasiswa/{mahasiswa_id}/riwayat-pembayaran', 'Mahasiswa\RiwayatPembayaranController@riwayatPembayaranIndex');
        $api->post('mahasiswa/{mahasiswa_id}/riwayat-pembayaran', 'Mahasiswa\RiwayatPembayaranController@riwayatPembayaranStore')->middleware('admin');

        /*
        |--------------------------------------------------------------------------
        | Dosen Endpoints
        |--------------------------------------------------------------------------
        | - List (DataTable) : GET -> /dosen
        | - Show : GET -> /dosen/{dosen_id}
        | - Update : PUT -> /dosen/{dosen_id}
        | - Ajar Dosen [List] : GET -> /dosen/{dosen_id}/ajar-dosen *dosen_id = id_reg_ptk
        | - Ajar Dosen [Store] : POST -> /dosen/{dosen_id}/ajar-dosen
        | - Ajar Dosen [Delete] : DELETE -> /dosen/{dosen_id}/ajar-dosen/{ajar_dosen_id}
        | - Riwayat Penugasan Dosen [List] : GET -> /dosen/{dosen_id}/penugasan
        | - Riwayat Pendidikan Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-pendidikan
        | - Riwayat Struktural Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-stuktural
        | - Riwayat Fungsional Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-fungsional
        | - Riwayat Kepangkatan Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-kepangkatan
        | - Riwayat Sertifikasi Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-sertifikasi
        |
         */

        $api->get('dosen', 'Dosen\DosenController@dosenIndex');
        $api->get('dosen/{dosen_id}', 'Dosen\DosenController@dosenShow');
        $api->put('dosen/{dosen_id}', 'Dosen\DosenController@dosenUpdate')->middleware('admin');
        $api->get('dosen/{dosen_id}/ajar-dosen', 'Dosen\AjarDosenController@ajarDosenIndex');
        $api->post('dosen/{dosen_id}/ajar-dosen', 'Dosen\AjarDosenController@ajarDosenStore')->middleware('admin');
        $api->delete('dosen/{dosen_id}/ajar-dosen/{ajar_dosen_id}', 'Dosen\AjarDosenController@ajarDosenDestroy')->middleware('admin');
        $api->get('dosen/{dosen_id}/penugasan', 'Dosen\PenugasanController@penugasanIndex');
        $api->get('dosen/{dosen_id}/riwayat-pendidikan', 'Dosen\RiwayatPendidikanController@riwayatPendidikanIndex');
        $api->get('dosen/{dosen_id}/riwayat-struktural', 'Dosen\RiwayatStrukturalController@riwayatStrukturalIndex');
        $api->get('dosen/{dosen_id}/riwayat-fungsional', 'Dosen\RiwayatFungsionalController@riwayatFungsionalIndex');
        $api->get('dosen/{dosen_id}/riwayat-kepangkatan', 'Dosen\RiwayatKepangkatanController@riwayatKepangkatanIndex');
        $api->get('dosen/{dosen_id}/riwayat-sertifikasi', 'Dosen\RiwayatSertifikasiController@riwayatSertifikasiIndex');

        /*
        |--------------------------------------------------------------------------
        | MataKuliah Endpoints
        |--------------------------------------------------------------------------
        |
        | - List (DataTable) : GET -> /mata-kuliah
        | - Create : POST -> /mata-kuliah
        | - Show : GET -> /mata-kuliah/{id_makul}
        | - Update : PUT -> /mata-kuliah/{id_makul}
        |
         */

        $api->get('mata-kuliah', 'MataKuliah\MataKuliahController@mataKuliahIndex');
        $api->post('mata-kuliah', 'MataKuliah\MataKuliahController@mataKuliahStore')->middleware('admin');
        $api->get('mata-kuliah/{mata_kuliah_id}', 'MataKuliah\MataKuliahController@mataKuliahShow');
        $api->put('mata-kuliah/{mata_kuliah_id}', 'MataKuliah\MataKuliahController@mataKuliahUpdate')->middleware('admin');


        /*
        |--------------------------------------------------------------------------
        | MataKuliahKurikulum Endpoints
        |--------------------------------------------------------------------------
        |
        | - List : GET -> /mata-kuliah-kurikulum
        |
         */

        $api->get('mata-kuliah-kurikulum', 'MataKuliahKurikulum\MataKuliahKurikulumController@mataKuliahKurikulumIndex');

        /*
        |--------------------------------------------------------------------------
        | Kurikulum Endpoints
        |--------------------------------------------------------------------------
        |
        | - List (DataTable) : Get -> /kurikulum
        | - Show : GET -> /kurikulum/{id_kurikulum}
        | - Update : PUT -> /kurikulum/{id_kurikulum}
        | - Mata Kuliah [List] : GET -> /kurikulum/{id_kurikulum}/mata-kuliah
        | - Mata Kuliah [Create] : POST -> /kurikulum/{id_kurikulum}/mata-kuliah
        | - Mata Kuliah [Show] : GET -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}
        | - Mata Kuliah [Update] : PUT -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}
        | - Mata Kuliah [Delete] : DELETE -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}
        |
         */

        $api->get('kurikulum', 'Kurikulum\KurikulumController@kurikulumIndex');
        $api->post('kurikulum', 'Kurikulum\KurikulumController@kurikulumStore')->middleware('admin');
        $api->get('kurikulum/{kurirkulum_id}', 'Kurikulum\KurikulumController@kurikulumShow');
        $api->put('kurikulum/{kurikulum_id}', 'Kurikulum\KurikulumController@kurikulumUpdate')->middleware('admin');
        $api->get('kurikulum/{kurikulum_id}/mata-kuliah', 'Kurikulum\MataKuliahController@mataKuliahIndex');
        $api->post('kurikulum/{kurikulum_id}/mata-kuliah', 'Kurikulum\MataKuliahController@mataKuliahStore')->middleware('admin');
        $api->get('kurikulum/{kurikulum_id}/mata-kuliah/{mata_kuliah_id}', 'Kurikulum\MataKuliahController@mataKuliahShow');
        $api->put('kurikulum/{kurikulum_id}/mata-kuliah/{mata_kuliah_id}', 'Kurikulum\MataKuliahController@mataKuliahUpdate')->middleware('admin');
        $api->delete('kurikulum/{kurikulum_id}/mata-kuliah/{mata_kuliah_id}', 'Kurikulum\MataKuliahController@mataKuliahDestroy')->middleware('admin');

        /*
        |--------------------------------------------------------------------------
        | Semester Endpoints
        |--------------------------------------------------------------------------
        |
        | - List (DataTable) : GET -> /semester
        | - Kelas Kuliah [List] : GET -> /semester/{semester_id}/kelas-kuliah
        | - Kelas Kuliah [Store] : POST -> /semester/{semester_id}/kelas-kuliah
        | - Kelas Kuliah [Destroy] : DELETE -> /semester/{semester_id}/kelas-kuliah/{kelas_kuliah_id}
        | - Kuliah Mahasiswa [List] : GET -> /semester/{semester_id}/kuliah-mahasiswa
        |
         */

        $api->get('semester', 'Semester\SemesterController@semesterIndex');
        $api->get('semester/{semester_id}/kelas-kuliah', 'Semester\KelasKuliahController@kelasKuliahIndex');
        $api->post('semester/{semester_id}/kelas-kuliah', 'Semester\KelasKuliahController@kelasKuliahStore')->middleware('admin');
        $api->delete('semester/{semester_id}/kelas-kuliah/{kelas_kuliah_id}', 'Semester\KelasKuliahController@kelasKuliahDestroy')->middleware('admin');
        $api->get('semester/{semester_id}/kuliah-mahasiswa', 'Semester\KuliahMahasiswaController@kuliahMahasiswaIndex');

        /*
        |--------------------------------------------------------------------------

        | Prodi Endpoints
        |--------------------------------------------------------------------------
        |
        | - List (DataTable) : GET -> /prodi
        | - Mata Kuliah [List] : GET -> /prodi/{prodi_id}/mata-kuliah
        | - Mata Kuliah [Show] : GET -> /prodi/{prodi_id}/mata-kuliah/{mata_kuliah_id}
        | - Mahasiswa Keluar (DO atau lulus) [List] : GET -> /prodi/{prodi_id}/mahasiswa-keluar
        | - Mahasiswa Keluar (DO atau lulus) [Store] : POST -> /prodi/{prodi_id}/mahasiswa-keluar
        | - Mahasiswa Keluar (DO atau lulus) [Show] : GET -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}
        | - Mahasiswa Keluar (DO atau lulus) [Update] : PUT -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}
        | - Mahasiswa Keluar (DO atau lulus) [Destroy] : DELETE -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}
        |
         */

        $api->get('prodi', 'Prodi\ProdiController@prodiIndex');
        $api->get('prodi/{prodi_id}/mata-kuliah', 'Prodi\MataKuliahController@mataKuliahIndex'); //belum dipake, mungkin akan dihapus
        $api->get('prodi/{prodi_id}/mata-kuliah/{mata_kuliah_id}', 'Prodi\MataKuliahController@mataKuliahShow');
        $api->get('prodi/{prodi_id}/mahasiswa-keluar', 'Prodi\MahasiswaKeluarController@mahasiswaKeluarIndex');
        $api->post('prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa_keluar_id}', 'Prodi\MahasiswaKeluarController@mahasiswaKeluarStore')->middleware('admin');
        $api->get('prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa_keluar_id}', 'Prodi\MahasiswaKeluarController@mahasiswaKeluarShow');
        $api->put('prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa_keluar_id}', 'Prodi\MahasiswaKeluarController@mahasiswaKeluarUpdate')->middleware('admin');
        $api->delete('prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa_keluar_id}', 'Prodi\MahasiswaKeluarController@mahasiswaKeluarDestroy')->middleware('admin');

        /*
        |--------------------------------------------------------------------------
        | KelasKuliah Endpoints
        |--------------------------------------------------------------------------
        |
        | - List (DataTable) : GET -> /kelas-kuliah
        | - Mahasiswa [List] : GET -> /kelas-kuliah/{id_kelas}/mahasiswa
        | - Dosen [List] : GET -> /kelas-kuliah/{id_kelas}/dosen
        | - Nilai [List] : GET -> /kelas-kuliah/{id_kelas}/nilai
        | - Nilai [Show] : GET -> /kelas-kuliah/{id_kelas}/nilai/{nilai_id}
        | - Nilai [Update] : PUT -> /kelas-kuliah/{id_kelas}/nilai/{nilai_id}
        |
         */

        $api->get('kelas-kuliah', 'KelasKuliah\KelasKuliahController@kelasKuliahIndex');

        $api->get('kelas-kuliah/{kelas_kuliah_id}/dosen', 'KelasKuliah\DosenController@dosenIndex');
        $api->post('kelas-kuliah/{kelas_kuliah_id}/dosen', 'KelasKuliah\DosenController@dosenStore')->middleware('admin');
        $api->delete('kelas-kuliah/{kelas_kuliah_id}/dosen/{id_ajar}', 'KelasKuliah\DosenController@dosenDestroy')->middleware('admin');

        $api->get('kelas-kuliah/{kelas_kuliah_id}/mahasiswa', 'KelasKuliah\MahasiswaController@mahasiswaIndex');
        $api->post('kelas-kuliah/{kelas_kuliah_id}/mahasiswa', 'KelasKuliah\MahasiswaController@mahasiswaStore')->middleware('admin');
        $api->delete('kelas-kuliah/{kelas_kuliah_id}/mahasiswa/{nilai_id}', 'KelasKuliah\MahasiswaController@mahasiswaDestroy')->middleware('admin');

        $api->get('kelas-kuliah/{kelas_kuliah_id}/nilai', 'KelasKuliah\NilaiController@nilaiIndex');
        $api->get('kelas-kuliah/{kelas_kuliah_id}/nilai/{nilai_id}', 'KelasKuliah\NilaiController@nilaiShow');
        $api->put('kelas-kuliah/{kelas_kuliah_id}/nilai/{nilai_id}', 'KelasKuliah\NilaiController@nilaiUpdate')->middleware('admin');
        $api->get('kelas-kuliah/{kelas_kuliah_id}/absen-isi-cetak', 'KelasKuliah\MahasiswaController@absenIsiCetak');
        $api->get('kelas-kuliah/{kelas_kuliah_id}/absen-cover-cetak', 'KelasKuliah\MahasiswaController@absenCoverCetak');

        /*
        |--------------------------------------------------------------------------
        | Dashboard Endpoints
        |--------------------------------------------------------------------------
        |
        | - Jumlah Mahasiswa : GET -> /dashboard-semester/{semester_id}/jumlah-mahasiswa
        |
         */

        $api->get('dashboard-semester/{semester_id}/jumlah-mahasiswa', 'Dashboard\Semester\MahasiswaController@getCountMahasiswaFromSemester');

    });

});
