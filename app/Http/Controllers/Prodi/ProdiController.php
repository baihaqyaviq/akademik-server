<?php

namespace FE_UNSIQ\Http\Controllers\Prodi;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Repositories\Prodi\ProdiRepository;
use Illuminate\Http\Request;

class ProdiController extends Controller
{

    protected $request;
    protected $prodiRepo;

    public function __construct(Request $request, ProdiRepository $prodiRepo)
    {
        $this->request = $request;
        $this->prodiRepo = $prodiRepo;
    }

    public function prodiIndex()
    {
        $prodiResource = $this->prodiRepo->getAll();

        return $this->responseWebixCollection($prodiResource);
    }

}