<?php

namespace FE_UNSIQ\Http\Controllers\Semester;

class KuliahMahasiswaController extends SemesterController
{

    /**
     * @param $semester_id
     * @return \Illuminate\Http\Response
     */
    public function kuliahMahasiswaIndex($semester_id)
    {
        $kuliahMahasiswaResource = $this->semesterRepo->getKuliahMahasiswa($semester_id);
        $kuliahMahasiswaResponse = $this->responseWebixCollection($kuliahMahasiswaResource, function($kuliah_mhs) {
            return [
                'id' => $kuliah_mhs->id_kuliah_pd,
                'id_smt' => $kuliah_mhs->id_smt,
                'nipd' => $kuliah_mhs->nipd,
                'nm_pd' => $kuliah_mhs->nm_pd,
                'nm_prodi' => $kuliah_mhs->nm_lemb,
                'mulai_smt' => $kuliah_mhs->mulai_smt,
                'nm_stat_mhs' => $kuliah_mhs->nm_stat_mhs,
                'ips' => $kuliah_mhs->ips,
                'ipk' => $kuliah_mhs->ipk,
                'sks_smt' => $kuliah_mhs->sks_smt,
                'sks_total' => $kuliah_mhs->sks_total,
            ];
        });

        return $kuliahMahasiswaResponse;
    }

}