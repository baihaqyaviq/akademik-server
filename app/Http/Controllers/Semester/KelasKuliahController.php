<?php

namespace FE_UNSIQ\Http\Controllers\Semester;

use FE_UNSIQ\Http\Requests\KelasKuliahRequest;
use FE_UNSIQ\Jobs\KelasKuliah\CreateKelasKuliahFromSemester;
use FE_UNSIQ\Jobs\KelasKuliah\DestroyKelasKuliahFromSemester;
use FE_UNSIQ\Services\Feeder\FeederClientException;
use Illuminate\Database\QueryException;

class KelasKuliahController extends SemesterController
{

    /**
     * @param $semester_id
     * @return \Illuminate\Http\Response
     */
    public function kelasKuliahIndex($semester_id)
    {
        $kelasKuliahResource = $this->semesterRepo->getKelasKuliah($semester_id);
        $kelasKUliahResponse = $this->responseWebixCollection($kelasKuliahResource, function ($kelas_kuliah) {
            return [
                'id' => $kelas_kuliah->id_kls,
                'id_smt' => $kelas_kuliah->id_smt,
                'nm_kls' => $kelas_kuliah->nm_kls,
                'kode_mk' => $kelas_kuliah->kode_mk,
                'nm_mk' => $kelas_kuliah->nm_mk,
                'sks_mk' => $kelas_kuliah->sks_mk,
                'kuota_kelas' => $kelas_kuliah->kuota_kelas,
            ];
        });

        return $kelasKUliahResponse;
    }

    /**
     * @param $semester_id
     * @return \Dingo\Api\Http\Response|void
     */
    public function kelasKuliahStore(KelasKuliahRequest $kelasKuliahRequest, $semester_id)
    {
        $job = new CreateKelasKuliahFromSemester($semester_id, $kelasKuliahRequest->all());

        if ($kelasJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($kelasJob, function($kelas){
                return [
                    'id' => $kelas->id_kls,
                ];
            });
        }

        /*try {
        } catch (FeederClientException $e) {
            return $this->response->error($e->getMessage(), 422);
        }*/

        return $this->response->error('Gagal mnambah kelas kuliah', 422);
    }

    /**
     * @param $semester_id
     * @param $kelas_kuliah_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function kelasKuliahDestroy($semester_id, $kelas_kuliah_id)
    {
        $job = new DestroyKelasKuliahFromSemester($kelas_kuliah_id);

        try {
            if ($this->dispatch($job)) {
                return $this->response->noContent()->setStatusCode(200);
            }

            return $this->response->error('Gagal menghapus kelas kuliah', 422);

        } catch (QueryException $e) {
            return $this->response->error($e->getMessage(), 422);
        }
    }

}
