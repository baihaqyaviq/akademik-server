<?php

namespace FE_UNSIQ\Http\Controllers\Dashboard;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Repositories\Dashboard\DashboardRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var DashboardRepository
     */
    protected $dashboardRepo;

    /**
     * DashboardController constructor.
     * @param Request $request
     * @param DashboardRepository $dashboardRepo
     */
    public function __construct(Request $request, DashboardRepository $dashboardRepo)
    {
        $this->request = $request;
        $this->dashboardRepo = $dashboardRepo;
    }

}