<?php

namespace FE_UNSIQ\Http\Controllers\MataKuliahKurikulum;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Repositories\MataKuliahKurikulum\MataKuliahKurikulumRepository;

class MataKuliahKurikulumController extends Controller
{
    protected $makulKurikulumRepo;

    public function __construct(MataKuliahKurikulumRepository $makulKurikulumRepo)
    {
        $this->makulKurikulumRepo = $makulKurikulumRepo;
    }

    public function mataKuliahKurikulumIndex()
    {
        $makulResource = $this->makulKurikulumRepo->getAll();

        return $this->responseWebixCollection($makulResource);
    }

}