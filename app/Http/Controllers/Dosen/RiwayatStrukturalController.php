<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class RiwayatStrukturalController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatStrukturalIndex($dosen_id)
    {
        $allRiwayatStruktural = $this->dosenRepo->getRiwayatStruktural($dosen_id);

        return $this->responseWebixCollection($allRiwayatStruktural);
    }

}