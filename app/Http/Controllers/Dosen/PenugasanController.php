<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class PenugasanController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function penugasanIndex($dosen_id)
    {
        $dosenPenugasanResource = $this->dosenRepo->getPenugasanDosen($dosen_id);
        $dosenPenugasanResponse = $this->responseWebixCollection($dosenPenugasanResource, function($dosen) {
            return [
                'id'                => $dosen->id_reg_ptk,
                'nm_thn_ajaran'     => $dosen->nm_thn_ajaran,
                'nm_prodi'          => $dosen->nm_lemb,
                'no_srt_tgs'        => $dosen->no_srt_tgs,
                'tgl_srt_tgs'       => $dosen->tgl_srt_tgs,
                'tmt_srt_tgs'       => $dosen->tmt_srt_tgs,
            ];
        });

        return $dosenPenugasanResponse;
    }
    
}