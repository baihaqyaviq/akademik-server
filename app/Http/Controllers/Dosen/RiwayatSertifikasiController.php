<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class RiwayatSertifikasiController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatSertifikasiIndex($dosen_id)
    {
        $allRiwayatSertifikasi = $this->dosenRepo->getRiwayatSertifikasi($dosen_id);

        return $this->responseWebixCollection($allRiwayatSertifikasi);
    }

}