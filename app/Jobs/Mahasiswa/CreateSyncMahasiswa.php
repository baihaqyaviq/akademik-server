<?php

namespace FE_UNSIQ\Jobs\Mahasiswa;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\MahasiswaProfil;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Feeder\WSClient;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateSyncMahasiswa extends Job implements SelfHandling
{

    protected $requestData;
    protected $feederClient;

    public function __construct($requestData)
    {
        $this->requestData = $requestData;
        $this->feederClient = new WSClient();
    }

    //TODO: jika statusSync false lokal aja
    public function handle()
    {
        $dataMhs = [
            'nm_pd' => $this->requestData['nm_pd'],
            'jk' => $this->requestData['jk'],
            'tgl_lahir' => $this->requestData['tgl_lahir'],
            'tmpt_lahir' => $this->requestData['tmpt_lahir'],
            'id_kk' => $this->requestData['id_kk'],
            'id_sp' => $this->requestData['id_sp'],
            'stat_pd' => $this->requestData['stat_pd'],
            'ds_kel' => $this->requestData['ds_kel'],
            'id_wil' => $this->requestData['id_wil'],
            'kewarganegaraan' => $this->requestData['kewarganegaraan'],
            'nm_ibu_kandung' => $this->requestData['nm_ibu_kandung'],
            'id_agama' => $this->requestData['id_agama'],
            'id_kebutuhan_khusus_ayah' => $this->requestData['id_kebutuhan_khusus_ayah'],
            'id_kebutuhan_khusus_ibu' => $this->requestData['id_kebutuhan_khusus_ibu'],
            'a_terima_kps' => $this->requestData['a_terima_kps'],
        ];

        // save data mahasiswa on feeder
        $resource = $this->feederClient->insertRecord('mahasiswa', $dataMhs);

        $dataMhsPt = [
            'id_pd' => $resource->result->id_pd,
            'nipd' => $this->requestData['nipd'],
            'id_sms' => $this->requestData['id_sms'],
            'id_sp' => $this->requestData['id_sp'],
            'id_jns_daftar' => $this->requestData['id_jns_daftar'],
            'tgl_masuk_sp' => $this->requestData['tgl_masuk_sp'],
        ];

        // save data mahasiswa_pt on feeder
        $resource2 = $this->feederClient->insertRecord('mahasiswa_pt', $dataMhsPt);

        $dataMhs['id_pd'] = $resource->result->id_pd;
        $dataMhsPt['id_reg_pd'] = $resource2->result->id_reg_pd;

        // save data mahasiswa on local server
        $mahasiswa = new MahasiswaProfil($dataMhs);
        $mahasiswa_pt = new Mahasiswa($dataMhsPt);

        $result = $mahasiswa->save() && $mahasiswa_pt->save();

        return $result;
    }

}