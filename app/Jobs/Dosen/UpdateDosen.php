<?php

namespace FE_UNSIQ\Jobs\Dosen;

use FE_UNSIQ\Eloquent\DosenProfil;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateDosen extends Job implements SelfHandling
{

    protected $dosen;
    protected $requestData;

    public function __construct($dosen_id, $requestData)
    {
        $this->dosen = DosenProfil::findOrFail($dosen_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        foreach (\Schema::getColumnListing($this->dosen->getTable()) as $fieldName) {
            if (array_key_exists($fieldName, $this->requestData)) {
                $this->dosen->$fieldName = $this->requestData[$fieldName];
            }
        }

        return $this->dosen->save();
    }

}