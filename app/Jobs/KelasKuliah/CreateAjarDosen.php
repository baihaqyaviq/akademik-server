<?php

namespace FE_UNSIQ\Jobs\KelasKuliah;

use Faker\Factory;
use FE_UNSIQ\Eloquent\AjarDosen;
use FE_UNSIQ\Eloquent\DosenProfil;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Webpatser\Uuid\Uuid;

class CreateAjarDosen extends Job implements SelfHandling
{

    protected $dosen;
    protected $requestData;
    protected $faker;

    public function __construct($requestData)
    {
        $this->requestData = $requestData;
        $this->dosen = DosenProfil::findOrFail($requestData['id_ptk'])->dosen;
        $this->faker = Factory::create();
    }

    public function handle()
    {
        $ajar_dosen = new AjarDosen([
            'id_ajar' => Uuid::generate(4),
            'id_subst' => NULL,
            'id_kls' => array_key_exists('id_kls', $this->requestData) ? $this->requestData['id_kls'] : NULL,
            'sks_subst_tot' => array_key_exists('sks_subst_tot', $this->requestData) ? $this->requestData['sks_subst_tot'] : NULL,
            'sks_tm_subst' => array_key_exists('sks_tm_subst', $this->requestData) ? $this->requestData['sks_tm_subst'] : NULL,
            'sks_prak_lap_subst' => array_key_exists('sks_prak_lap_subst', $this->requestData) ? $this->requestData['sks_prak_lap_subst'] : NULL,
            'sks_sim_subst' => array_key_exists('sks_sim_subst', $this->requestData) ? $this->requestData['sks_sim_subst'] : NULL,
            'jml_tm_renc' => array_key_exists('jml_tm_renc', $this->requestData) ? $this->requestData['jml_tm_renc'] : NULL,
            'jml_tm_real' => array_key_exists('jml_tm_real', $this->requestData) ? $this->requestData['jml_tm_real'] : NULL,
        ]);

        $ajar_dosen->dosen()->associate($this->dosen);

        if ($ajar_dosen->save()){
            return $ajar_dosen;
        }
        return false;
    }

}