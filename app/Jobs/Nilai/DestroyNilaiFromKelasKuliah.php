<?php

namespace FE_UNSIQ\Jobs\Nilai;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DestroyNilaiFromKelasKuliah extends Job implements SelfHandling
{

    protected $mahasiswa;
    protected $nilai;

    public function __construct(KelasKuliah $kelasKuliah, $nilai_id)
    {
        $this->nilai = $kelasKuliah->nilai()->findOrFail($nilai_id);
    }

    public function handle()
    {
        if ($this->nilai->delete()){
            return $this->nilai;
        }
        return false;

    }
}