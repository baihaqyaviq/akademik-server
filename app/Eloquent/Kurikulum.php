<?php


namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;


class Kurikulum extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'kurikulum';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_kurikulum_sp';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id_kurikulum_sp',
        'nm_kurikulum_sp',
        'jml_sem_normal',
        'jml_sks_lulus',
        'jml_sks_wajib',
        'jml_sks_pilihan',
        'id_sms',
        'id_jenj_didik',
        'id_smt_berlaku'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mata_kuliah()
    {
        return $this->belongsToMany(MataKuliah::class, 'mata_kuliah_kurikulum', 'id_kurikulum_sp', 'id_mk')
            ->withPivot(
                'smt',
                'sks_mk',
                'sks_tm',
                'sks_prak',
                'sks_prak_lap',
                'sks_sim',
                'a_wajib',
                'last_update',
                'soft_delete',
                'last_sync',
                'id_updater',
                'last_update_local'
            );
    }
}