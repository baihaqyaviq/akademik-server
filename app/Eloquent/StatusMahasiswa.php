<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class StatusMahasiswa extends Model
{
    
	/**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'status_mahasiswa';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_stat_mhs';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;

	/**
	 * Retrieve mahasiswa yang terkait dengan model
	 * @return mixed 
	 */
	public function mahasiswa()
	{
		return $this->hasMany(Mahasiswa::class, 'stat_pd');
	}

}
