<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class IkatanKerjaDosen extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'ikatan_kerja_dosen';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_ikatan_kerja';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Retrieve mahasiswa yang terkait dengan model
     * @return mixed
     */
    public function dosen()
    {
        return $this->hasMany(DosenProfil::class, 'id_ikatan_kerja');
    }
}
