<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'mata_kuliah';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_mk';

    /**
     * @var array
     */
    protected $fillable = [
        'id_mk',
        'id_sms',
        'id_jenj_didik',
        'kode_mk',
        'nm_mk',
        'jns_mk',
        'kel_mk',
        'sks_mk',
        'sks_tm',
        'sks_prak',
        'sks_prak_lab',
        'sks_sim',
        'metode_pelaksanaan_kuliah',
        'a_sap',
        'a_silabus',
        'a_bahan_ajar',
        'acara_prak',
        'a_diklat',
        'tgl_mulai_efektif',
        'tgl_akhir_efektif'
    ];

    /**
     * BelongsTo sms
     * @return mixed
     */
    public function sms()
    {
        return $this->belongsTo(SMS::class, 'id_sms');
    }

    /**
     * HasMany kelas_kuliah
     * @return mixed
     */
    public function kelas_kuliah()
    {
        return $this->hasMany(KelasKuliah::class, 'id_mk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function kurikulum()
    {
        return $this->belongsToMany(Kurikulum::class, 'mata_kuliah_kurikulum', 'id_mk', 'id_kurikulum_sp');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenis_matakuliah()
    {
        return $this->belongsTo(JenisMatakuliah::class, 'jns_mk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kel_matakuliah()
    {
        return $this->belongsTo(KelMatakuliah::class, 'kel_mk');
    }


}
