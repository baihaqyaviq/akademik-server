<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class JenisKeluar extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'jenis_keluar';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_jns_keluar';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;
}
