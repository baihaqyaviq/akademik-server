<?php

namespace FE_UNSIQ\Repositories\Dashboard;

use DB;
use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Repositories\Repository;
use FE_UNSIQ\Services\Semester\SemesterFactory;
use FE_UNSIQ\Services\Webix\Resource;
use Illuminate\Database\Eloquent\Collection;

class DashboardRepository extends Repository
{

    use QueryGenerator;

    /**
     * @var
     */
    protected $semester_id;

    /**
     * @var Mahasiswa
     */
    protected $mahasiswa;

    /**
     * DashboardRepository constructor.
     * @param Mahasiswa $mahasiswa
     */
    public function __construct(Mahasiswa $mahasiswa)
    {
        $this->semester_id = (new SemesterFactory())->generateTahunSmtForNow();
        $this->mahasiswa = $mahasiswa;
    }

    /**
     * @param $semester_id
     * @return Collection
     */
    public function getCountsMahasiswaFromSemester($semester_id)
    {
        $this->semester_id = $semester_id;

        // count mahasiswa lulus
        $sql = $this->generateMahasiswaKeluarQuery('jumlah_mahasiswa_lulus', ' WHERE stat_pd = "L" AND keluar_tahun_ini = 1');
        $countMahasiswaLulus = DB::select(DB::raw($sql))[0];

        // count mahasiswa DO
        $sql = $this->generateMahasiswaKeluarQuery('jumlah_mahasiswa_DO', ' WHERE stat_pd = "D" AND keluar_tahun_ini = 1');
        $countMahasiswaDO = DB::select(DB::raw($sql))[0];

        // count mahasiswa Registrasi
        $sql = $this->generateMahasiswaRegistrasiQuery('jumlah_mahasiswa_register');
        $countMahasiswaRegister = DB::select(DB::raw($sql))[0];

        // count mahasiwa Cuti
        $sql = $this->generateMahasiswaKeluarQuery('jumlah_mahasiswa_cuti', ' WHERE stat_pd = "C" AND keluar_tahun_ini = 1');
        $countMahasiswaCuti = DB::select(DB::raw($sql))[0];

        return new Collection([
            'jumlah_mahasiswa_lulus' => $countMahasiswaLulus->jumlah_mahasiswa_lulus,
            'jumlah_mahasiswa_do' => $countMahasiswaDO->jumlah_mahasiswa_DO,
            'jumlah_mahasiswa_cuti' => $countMahasiswaCuti->jumlah_mahasiswa_cuti,
            'jumlah_mahasiswa_registrasi' => $countMahasiswaRegister->jumlah_mahasiswa_register,
            'jumlah_mahasiswa_register_laki_laki' => $this->countMahasiswaByGender('L'),
            'jumlah_mahasiswa_register_perempuan' => $this->countMahasiswaByGender('P'),
        ]);
    }

    /**
     * @param $gender
     * @return mixed
     */
    protected function countMahasiswaByGender($gender)
    {
        // count mahasiswa by gender
        $sql = $this->generateMahasiswaRegistrasiQuery('jumlah_mahasiswa', ' AND jk = "' . $gender . '"');
        $countMahasiswaByGender= DB::select(DB::raw($sql))[0];

        return $countMahasiswaByGender->jumlah_mahasiswa;
    }

}