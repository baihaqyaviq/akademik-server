<?php

namespace FE_UNSIQ\Repositories\Mahasiswa;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\Semester;
use FE_UNSIQ\Repositories\Repository;
use FE_UNSIQ\Services\Semester\SemesterFactory;

class MahasiswaRepository extends Repository
{

    /**
     * @var Mahasiswa
     */
    protected $mahasiswa;

    /**
     * MahasiswaRepository constructor.
     * @param Mahasiswa $mahasiswa
     */
    public function __construct(Mahasiswa $mahasiswa)
    {
        $this->mahasiswa = $mahasiswa; //tabel mahasiswa_pt
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $mahasiswaBuilder = $this->mahasiswa->select()
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->join('status_mahasiswa', 'status_mahasiswa.id_stat_mhs', '=', 'mahasiswa.stat_pd')
            ->join('sms', 'sms.id_sms', '=', 'mahasiswa_pt.id_sms')
            ->orderBy('nipd');

        $mahasiswa = $this->makeDynamicResource($mahasiswaBuilder);

        return $mahasiswa;
    }

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function show($mahasiswa_id)
    {
        $mahasiswa = $this->mahasiswa->select()
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->join('sms', 'mahasiswa_pt.id_sms', '=', 'sms.id_sms')
            ->where('id_reg_pd', '=', $mahasiswa_id)
            ->firstOrFail();

        return $mahasiswa;
    }

    /**
     * @param $mahasiswa_id
     * @return Resource
     */
    public function getKrs($mahasiswa_id, $smt_mahasiswa = false)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $nilaiBuilder = $mahasiswa->nilai()
            ->join('kelas_kuliah', 'kelas_kuliah.id_kls', '=', 'nilai.id_kls')
            ->join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->orderBy('mata_kuliah.kode_mk', 'asc')
            ->getQuery();

        if ($smt_mahasiswa)
            $nilaiBuilder->where('nilai.smt', '=', $smt_mahasiswa);

        $nilaiResource = $this->makeDynamicResource($nilaiBuilder);

        return $nilaiResource;
    }

    public function getKrsDitawarkan($mahasiswa_id)
    {
        $makulTawar = $this->getKrsHer($mahasiswa_id)->getItems();

        $ids = [
            'kode_mk' => '',
            'id_mk' => ''
        ];
        foreach ($makulTawar as $item) {
            $ids['id_mk'][] = $item->id_mk;
            $ids['kode_mk'][] = $item->kode_mk;
        }

        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $builder = KelasKuliah::join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->where('id_smt', '=', Semester::where('is_active', '=', 1)->first()->id_smt)
            ->where('kelas_kuliah.id_sms', '=', $mahasiswa->id_sms)
            ->where('kelas_kuliah.nm_kls', '=', $mahasiswa->nm_rombel);
//            ->whereNotIn('mata_kuliah.kode_mk', $ids['kode_mk']);

        $nilaiResource = $this->makeDynamicResource($builder);

        return $nilaiResource;
    }

    public function getKrsHer($mahasiswa_id)
    {
        /*$makulTawar = $this->getKrsDitawarkan($mahasiswa_id)->getItems();

        $ids = [];
        foreach ($makulTawar as $item) {
            $ids['id_mk'][] = $item->id_mk;
            $ids['kode_mk'][] = $item->kode_mk;
        }

        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $builder = $mahasiswa->nilai()->getQuery()
            ->join('kelas_kuliah', 'kelas_kuliah.id_kls', '=', 'nilai.id_kls')
            ->join('mata_kuliah', 'kelas_kuliah.id_mk', '=', 'mata_kuliah.id_mk')
            ->where('nilai_huruf', '!=', 'A')
            ->where('nilai_huruf', '!=', 'B')
//            ->whereIn('nilai.id_mk', $ids['id_mk'])
            ->whereIn('nilai.kode_mk', $ids['kode_mk']);

        $nilaiResource = $this->makeDynamicResource($builder);

        return $nilaiResource;*/

//        select * from `kelas_kuliah`
//        inner join `mata_kuliah` on `mata_kuliah`.`id_mk` = `kelas_kuliah`.`id_mk`
//        inner join `nilai` on `nilai`.`kode_mk` = `mata_kuliah`.`kode_mk`
//        where `kelas_kuliah.id_smt` = 20152
//        and `kelas_kuliah`.`id_sms` = e8372392-ac46-491d-b200-735e053f18e8
//        and `kelas_kuliah`.`nm_kls` = 01 limit 25

        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $builder = KelasKuliah::join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->join('nilai', 'nilai.kode_mk', '=', 'mata_kuliah.kode_mk')
            ->where('kelas_kuliah.id_smt', '=', Semester::where('is_active', '=', 1)->first()->id_smt)
            ->where('kelas_kuliah.id_sms', '=', $mahasiswa->id_sms)
            ->where('kelas_kuliah.nm_kls', '=', $mahasiswa->alokasi_kelas)
            ->where('nilai.id_reg_pd', '=', $mahasiswa->id_reg_pd)
            ->where('nilai.nilai_huruf', '!=', 'A')
            ->where('nilai.nilai_huruf', '!=', 'B');

        $nilaiResource = $this->makeDynamicResource($builder);

        return $nilaiResource;
    }

    public function getKrsDiambil($mahasiswa_id)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $builder = $mahasiswa->nilai()->getQuery()
            ->join('kelas_kuliah', 'kelas_kuliah.id_kls', '=', 'nilai.id_kls')
            ->join('mata_kuliah', 'kelas_kuliah.id_mk', '=', 'mata_kuliah.id_mk')
            ->where('kelas_kuliah.id_smt', '=', (new SemesterFactory())->generateTahunSmtForNow());
        $nilaiResource = $this->makeDynamicResource($builder);

        return $nilaiResource;
    }

    /**
     * @param $mahasiswa_id
     * @param $nilai_id
     * @return mixed
     */
    public function showKrs($mahasiswa_id, $nilai_id)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $nilai = $mahasiswa->nilai()->findOrFail($nilai_id);

        return $nilai;
    }

    /**
     * @param $mahasiswa_id
     * @return \FE_UNSIQ\Services\Webix\Resource
     */
    public function getKuliahMahasiswa($mahasiswa_id)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $kuliahMahasiswaBuilder = $mahasiswa->kuliah_mahasiswa()->getQuery();
        $kuliahMahasiswaResource = $this->makeDynamicResource($kuliahMahasiswaBuilder);

        return $kuliahMahasiswaResource;
    }

    /**
     * @param $mahasiswa_id
     * @return \FE_UNSIQ\Services\Webix\Resource
     */
    public function getRiwayatPendidikan($mahasiswa_id)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $id_pd = $mahasiswa->mahasiswa_profil->id_pd;
        $riwayatPendidikanBuilder = $this->mahasiswa->where('id_pd', '=', $id_pd);
        $riwayatPendidikanResource = $this->makeDynamicResource($riwayatPendidikanBuilder);

        return $riwayatPendidikanResource;
    }

    /**
     * @param $mahasiswa_id
     * @return Resource
     */
    public function getHistoriPembayaran($mahasiswa_id)
    {
        $mahasiswa = $this->mahasiswa->findOrFail($mahasiswa_id);
        $historiPembayaranBuilder = $mahasiswa->histori_pembayaran()->getQuery()
            ->join('kategori_bayar', 'histori_pembayaran.id_kategori_bayar', '=', 'kategori_bayar.id_kategori_bayar');

        $historiPembayaranResource = $this->makeDynamicResource($historiPembayaranBuilder);

        return $historiPembayaranResource;
    }

    public function getGrafikMahasiswa()
    {
        $mahasiswaBuilder = $this->mahasiswa->select(
            \DB::raw('nm_lemb, singkatan, regpd_mulai_smt, 
            count(IF(regpd_id_sms = \'dd54cb24-01bc-4f01-a391-4a5efbc0f683\',mahasiswa.id_pd,NULL)) AS j_mpdi,
            count(IF(regpd_id_sms = \'c32fa85a-6d6c-4aa2-bc62-24c7b0d29871\',mahasiswa.id_pd,NULL)) AS j_msi'))
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->join('sms', 'sms.id_sms', '=', 'mahasiswa_pt.id_sms')
            ->where('stat_pd', '=', 'A')
            ->groupBy('regpd_id_sms')
            ->groupBy('regpd_mulai_smt');

        $mahasiswaGrafik = $this->makeDynamicResource($mahasiswaBuilder);

        return $mahasiswaGrafik;
    }

}
