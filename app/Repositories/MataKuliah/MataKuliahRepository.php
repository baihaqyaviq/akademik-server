<?php

namespace FE_UNSIQ\Repositories\MataKuliah;

use FE_UNSIQ\Eloquent\MataKuliah;
use FE_UNSIQ\Repositories\Repository;

class MataKuliahRepository extends Repository
{
    /**
     * @var MataKuliah
     */
    protected $mataKuliah;

    /**
     * MataKuliahRepository constructor.
     * @param MataKuliah $mataKuliah
     */
    public function __construct(MataKuliah $mataKuliah)
    {
        $this->mataKuliah = $mataKuliah;
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $mataKuliahBuilder = $this->mataKuliah->select()
            ->join('sms', 'sms.id_sms', '=', 'mata_kuliah.id_sms');
        $mataKuliahResource = $this->makeDynamicResource($mataKuliahBuilder);

        return $mataKuliahResource;
    }

    /**
     * @param $mata_kuliah_id
     * @return mixed
     */
    public function show($mata_kuliah_id)
    {
        $mataKuliah = $this->mataKuliah->findOrFail($mata_kuliah_id);

        return $mataKuliah;
    }
}