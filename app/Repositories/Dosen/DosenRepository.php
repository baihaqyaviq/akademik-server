<?php

namespace FE_UNSIQ\Repositories\Dosen;

use FE_UNSIQ\Eloquent\DosenProfil;
use FE_UNSIQ\Eloquent\DosenPt;
use FE_UNSIQ\Repositories\Repository;
use FE_UNSIQ\Services\Webix\Resource;
use Illuminate\Database\Eloquent\Collection;

class DosenRepository extends Repository
{

    /**
     * @var DosenPt
     */
    protected $dosenPt;

    /**
     * @var DosenProfil
     */
    protected $dosenProfil;

    /**
     * DosenRepository constructor.
     * @param DosenPt $dosenPt
     * @param DosenProfil $dosenProfil
     */
    public function __construct(DosenPt $dosenPt, DosenProfil $dosenProfil)
    {
        $this->dosenPt = $dosenPt; //table: dosen_pt
        $this->dosenProfil = $dosenProfil; //table: dosen
    }


    /**
     * get all dosen universitas
     * @return Resource
     */
    public function getAllDosen()
    {
        $dosenUnivBuilder = $this->dosenProfil->select()
            ->leftjoin('dosen_pt', 'dosen.id_ptk', '=', 'dosen_pt.id_ptk')
            ->leftjoin('sms', 'dosen_pt.id_sms', '=', 'sms.id_sms')
            ->leftjoin('ikatan_kerja_dosen', 'dosen.id_ikatan_kerja', '=', 'ikatan_kerja_dosen.id_ikatan_kerja')
            ->leftjoin('agama', 'dosen.id_agama', '=', 'agama.id_agama')
            ->leftjoin('status_kepegawaian', 'dosen.id_stat_pegawai', '=', 'status_kepegawaian.id_stat_pegawai')
            ->leftjoin('status_keaktifan_pegawai', 'dosen.id_stat_aktif', '=', 'status_keaktifan_pegawai.id_stat_aktif')
            ->where('dosen.id_sp', '=', '6d0ac338-04d4-40e7-82ed-efbf5b66e956')
            // ->where('id_thn_ajaran', '>=', date('Y') - 1)
            ->orderBy('id_thn_ajaran', 'desc')
            ->orderBy('nm_ptk', 'asc');

        $dosenResource = $this->makeDynamicResource($dosenUnivBuilder);
        return $dosenResource;
    }

    /**
     * get all dosen pt
     * @return Resource
     */
    public function getAll()
    {
        $dosenBuilder = $this->dosenPt->select()
            ->join('dosen', 'dosen.id_ptk', '=', 'dosen_pt.id_ptk')
            ->join('sms', 'sms.id_sms', '=', 'dosen_pt.id_sms')
            ->join('status_kepegawaian', 'status_kepegawaian.id_stat_pegawai', '=', 'dosen.id_stat_pegawai')
//            ->where('id_thn_ajaran','=', date('Y'));
//            ->groupBy('dosen_pt.id_ptk')
//            ->groupBy('dosen_pt.id_thn_ajaran')
            ->orderBy('id_thn_ajaran', 'DESC');

        $dosenResource = $this->makeDynamicResource($dosenBuilder);
        return $dosenResource;
    }

    /**
     * @param $dosen_id
     * @return mixed
     */
    public function show($dosen_id)
    {
        $dosen = $this->dosenPt->select()
            ->join('dosen', 'dosen_pt.id_ptk', '=', 'dosen.id_ptk')
            ->where('dosen.id_ptk', '=', $dosen_id)
            ->firstOrFail();

        return $dosen;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getAjarDosen($dosen_id)
    {
        $dosen = $this->dosenProfil->findOrFail($dosen_id);
        $dosen_pt = $dosen->dosen()->get();
        $ajarDosen = [];
        foreach ($dosen_pt as $dosen) {
            $ajarDosen[] = $dosen->ajar_dosen()
                ->leftJoin('kelas_kuliah', 'ajar_dosen.id_kls', '=', 'kelas_kuliah.id_kls')
                ->leftJoin('mata_kuliah', 'kelas_kuliah.id_mk', '=', 'mata_kuliah.id_mk')
                ->join('mata_kuliah_kurikulum', 'mata_kuliah.id_mk', '=', 'mata_kuliah_kurikulum.id_mk')
                ->get();
        }

        $ajarDosen = new Collection($ajarDosen);
        $newAjar = [];
        foreach ($ajarDosen as $ajar) {
            foreach ($ajar as $j) {
                $newAjar[] = $j;
            }
        }

        $resource = new Resource();
        $resource->setItems(new Collection($newAjar));

        return $resource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getPenugasanDosen($dosen_id)
    {
        $dosenBuilder = $this->dosenPt->select()
            ->join('sms', 'sms.id_sms', '=', 'dosen_pt.id_sms')
            ->join('tahun_ajaran', 'tahun_ajaran.id_thn_ajaran', '=', 'dosen_pt.id_thn_ajaran')
            ->where('id_ptk', '=', $dosen_id)
            ->orderBy('dosen_pt.id_thn_ajaran', 'DESC');
        $dosenResource = $this->makeDynamicResource($dosenBuilder);

        return $dosenResource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getRiwayatPendidikan($dosen_id)
    {
        $riwayatPendidikanBuilder = $this->dosenProfil->findOrFail($dosen_id)
            ->riwayat_pendidikan()
            ->getQuery();
        $riwayatPendidikanResource = $this->makeDynamicResource($riwayatPendidikanBuilder);

        return $riwayatPendidikanResource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getRiwayatStruktural($dosen_id)
    {
        $riwayatSrukturalBuilder = $this->dosenProfil->findOrFail($dosen_id)->riwayat_struktural()->getQuery();
        $riwayatSrukturalResource = $this->makeDynamicResource($riwayatSrukturalBuilder);

        return $riwayatSrukturalResource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getRiwayatFungsional($dosen_id)
    {
        $riwayatFungsionalBuilder = $this->dosenProfil->findOrFail($dosen_id)->riwayat_fungsional()->getQuery();
        $riwayatFungsionalResource = $this->makeDynamicResource($riwayatFungsionalBuilder);

        return $riwayatFungsionalResource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getRiwayatKepangkatan($dosen_id)
    {
        $riwayatKepangkatanBuilder = $this->dosenProfil->findOrFail($dosen_id)->riwayat_kepangkatan()->getQuery();
        $riwayatKepangkatanResource = $this->makeDynamicResource($riwayatKepangkatanBuilder);

        return $riwayatKepangkatanResource;
    }

    /**
     * @param $dosen_id
     * @return Resource
     */
    public function getRiwayatSertifikasi($dosen_id)
    {
        $riwayatSertifikasiBuilder = $this->dosenProfil->findOrFail($dosen_id)->riwayat_sertifikasi()->getQuery();
        $riwayatSertifikasiResource = $this->makeDynamicResource($riwayatSertifikasiBuilder);

        return $riwayatSertifikasiResource;
    }
}