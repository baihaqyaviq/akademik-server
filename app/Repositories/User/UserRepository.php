<?php

namespace FE_UNSIQ\Repositories\User;

use FE_UNSIQ\Eloquent\User;
use FE_UNSIQ\Repositories\Repository;

class UserRepository extends Repository
{
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    public function getAll()
    {
        $user = $this->makeDynamicResource(User::select());

        return $user;
    }

    public function show($user_id)
    {
        $user = $this->user->findOrFail($user_id);

        return $user;
    }
}