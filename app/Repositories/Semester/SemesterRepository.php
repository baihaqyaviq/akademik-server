<?php

namespace FE_UNSIQ\Repositories\Semester;

use FE_UNSIQ\Eloquent\Semester;
use FE_UNSIQ\Repositories\Repository;

class SemesterRepository extends Repository
{
    /**
     * @var Semester
     */
    protected $semester;

    /**
     * SemesterRepository constructor.
     * @param Semester $semester
     */
    public function __construct(Semester $semester)
    {
        $this->semester = $semester;
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $semesterBuilder = $this->semester
            ->select()
            ->where('a_periode_aktif','=','1')
            ->orderBy('id_smt', 'desc');
        $semesterResource = $this->makeDynamicResource($semesterBuilder);

        return $semesterResource;
    }

    /**
     * @param $semester_id
     * @return Resource
     */
    public function getKelasKuliah($semester_id)
    {
        $kelasKuliahBuilder = $this->semester->findOrFail($semester_id)->kelas_kuliah()
            ->join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->join('mata_kuliah_kurikulum', 'mata_kuliah.id_mk', '=', 'mata_kuliah_kurikulum.id_mk')
            ->orderBy('kode_mk')
            ->getQuery();

        $kelasKuliahResource = $this->makeDynamicResource($kelasKuliahBuilder);

        return $kelasKuliahResource;
    }

    /**
     * @param $semester_id
     * @return Resource
     */
    public function getKuliahMahasiswa($semester_id)
    {
        $kuliahMahasiswaBuilder = $this->semester->findOrFail($semester_id)->kuliah_mahasiswa()
            ->join('mahasiswa_pt', 'mahasiswa_pt.id_reg_pd', '=', 'kuliah_mahasiswa.id_reg_pd')
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->join('sms', 'sms.id_sms', '=', 'mahasiswa_pt.id_sms')
            ->join('status_mahasiswa', 'status_mahasiswa.id_stat_mhs', '=', 'mahasiswa.stat_pd')
            ->getQuery();
        $kuliahMahasiswaResource = $this->makeDynamicResource($kuliahMahasiswaBuilder);

        return $kuliahMahasiswaResource;
    }
}